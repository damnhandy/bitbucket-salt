# Bitbucket Server & Nginx Containers Managed by SaltStack

This project uses Atlassian's [Bitbucket Server](https://bitbucket.org/atlassian/docker-atlassian-bitbucket-server/overview)
behind an Nginx reverse proxy. It's using [SaltStack's](http://saltstack.com/community/)
[dockerng](https://docs.saltstack.com/en/latest/ref/states/all/salt.states.dockerng.html) to manage
the configuration files and the containers themselves.

I'm using a [Vagrant Box](https://docs.vagrantup.com/v2/) to test everything locally.
In order for this to work properly, you need to create a pillar named
`docker.sls` file in `/srv/pillar`. This file needs to configured as described
in the authentication section of the
[dockerng module](https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.dockerng.html#docker-authentication)

After running `vagrant up`, you will be able to access the Nginx/Bitbucket instance on:

http://localhost:8080/

Still to do:

* Automating installation of the license file.
* JDBC Driver configuration
* Additional Plugins
* SSL for Nginx
