# Install docker-engine and make sure the service is running
docker:
  pkg.installed:
    - name: docker-io
  service.running:
    - name: docker
    - enable: True

# Ensure Python is installed
python:
  pkg.installed:
    - name: python

# Make sure pip is installed. Pip requires Python, so we specify that
python-pip:
  pkg.installed:
    - name: python-pip
    - require:
      - pkg: python

# The Salt dockerng states need docker-py from Pip. Docker-py requires pip
docker-py:
  pip.installed:
    - reload_modules: True
    - require:
      - pkg: python-pip

# docker-compose just needs docker and pip
docker-compose:
  pip.installed:
    - name: docker-compose
    - reload_modules: True
    - require:
      - pkg: python-pip
      - pkg: docker
