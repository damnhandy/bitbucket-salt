create_user:
  user.present:
    - name: daemon
    - system: True
    - fullname: Bitbucket User
    - home: False
    - shell: /dev/null
    - groups:
      - daemon


# Create the bitbucket home directory
/opt/bitbucket_home/shared/server.xml:
  file.managed:
    - source: salt://files/bitbucket/server.xml
    - user: daemon
    - group: daemon
    - file_mode: 744
    - dir_mode: 755
    - makedirs: True
    - require:
      - user: daemon
    - recurse:
      - user
      - group
      - mode

# Prepare the bitbucket.properties using values from an externalized pillar
/opt/bitbucket_home/shared/bitbucket.properties:
  file.managed:
    - source: salt://files/bitbucket/bitbucket.properties
    - user: daemon
    - group: daemon
    - file_mode: 744
    - dir_mode: 755
    - makedirs: True
    - template: jinja
    - require:
      - user: daemon
    - recurse:
      - user
      - group
      - mode
    - defaults:
        license: {{ pillar['bitbucket']['setup']['license'] }}
        display_name: {{ pillar['bitbucket']['setup']['display_name'] }}
        base_url: {{ pillar['bitbucket']['setup']['base_url'] }}
        username: {{ pillar['bitbucket']['setup']['username'] }}
        password: {{ pillar['bitbucket']['setup']['password'] }}
        user_display_name: {{ pillar['bitbucket']['setup']['user_display_name'] }}
        email_address: {{ pillar['bitbucket']['setup']['email_address'] }}

# Download the JDBC Driver
/opt/bitbucket_home/lib/postgresql-9.4-1206-jdbc42.jar:
  file.managed:
    - user: daemon
    - group: daemon
    - file_mode: 744
    - dir_mode: 755
    - makedirs: True
    - source: http://search.maven.org/remotecontent?filepath=org/postgresql/postgresql/9.4-1206-jdbc42/postgresql-9.4-1206-jdbc42.jar
    - source_hash: https://repo1.maven.org/maven2/org/postgresql/postgresql/9.4-1206-jdbc42/postgresql-9.4-1206-jdbc42.jar.md5
    - require:
      - user: daemon


/opt/apps/conf/nginx.conf:
  file.managed:
    - makedirs: True
    - user: root
    - group: root
    - mode: 0644
    - source: salt://files/nginx/nginx.conf
    - template: jinja
    - defaults:
        SERVER_NAME: "localhost"
        INTERNAL_NAME: "bitbucket"
        INTERNAL_PORT: "7990"

# Check to see if the image is present. If it's not, it'll be pulled
# and installed.
nginx:1.9.7:
  dockerng.image_present:
    - require:
      - sls: docker
      - file: /opt/apps/conf/nginx.conf

# Ensure bitbucket is present
atlassian/bitbucket-server:
  dockerng.image_present:
    - require:
      - sls: docker
      - file: /opt/bitbucket_home/shared/server.xml

# Set permissions on bitbucket_home
bitbucket_permissions:
  cmd.run:
    - name: docker run -u root -v /opt/bitbucket_home:/var/atlassian/application-data/bitbucket atlassian/bitbucket-server chown -R daemon /var/atlassian/application-data/bitbucket
    - require:
      - file: /opt/bitbucket_home/shared/server.xml
      - dockerng: atlassian/bitbucket-server

# Run the Nginx container. The watch_action is set to SIGHUP as this simply reloads the config file when it changes.
nginxcontainer:
  dockerng.running:
    - name: nginx
    - image: nginx:1.9.7
    - binds: /opt/apps/conf/nginx.conf:/etc/nginx/nginx.conf:ro
    - links: bitbucket:bitbucket
    - port_bindings:
      - 80:80
      - 443:443
    - watch_action: SIGHUP
    - watch:
      - file: /opt/apps/conf/nginx.conf
    - restart_policy: always
    - require:
      - dockerng: nginx:1.9.7
      - dockerng: bitbucket_container

# Run the Bitbucket Server container. The watch_action is set to 'force' as this cause the container to ge rebuilt
# when the config changes. Bitbucket server doesn't seem to handle SIGHUP events, thus the entire process needs to
# restarted when the configuration changes. . When this container is rebuilt, it will also trigger a rebuild of
# the nginx container.
bitbucket_container:
  dockerng.running:
    - name: bitbucket
    - image: atlassian/bitbucket-server
    - user: daemon
    - binds:
      - /opt/bitbucket_home:/var/atlassian/application-data/bitbucket:rw
    - port_bindings:
      - 7990:7990
      - 7999:7999
    - watch_action: 'force'
    - watch:
      - file: /opt/bitbucket_home/shared/server.xml
      - file: /opt/bitbucket_home/shared/bitbucket.properties
      - file: /opt/bitbucket_home/lib/postgresql-9.4-1206-jdbc42.jar
    - restart_policy: always
    - require:
      - file: /opt/bitbucket_home/lib/postgresql-9.4-1206-jdbc42.jar
      - file: /opt/bitbucket_home/shared/bitbucket.properties
      - file: /opt/bitbucket_home/shared/server.xml
      - dockerng: atlassian/bitbucket-server
      - user: daemon
